<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action( 'landingpress_body_before' ); ?>
<div class="site-canvas" canvas="container">
<?php do_action( 'landingpress_page_before' ); ?>
<div id="page" class="site-container">
<?php if ( landingpress_is_header_active() ) : ?>
	<header id="masthead" class="site-header">
		<?php 
		$site_branding_class = 'site-branding clearfix';
		if ( is_active_sidebar( 'header' ) ) {
			$site_branding_class .= ' site-header-widget-active';
		}
		if ( $align = get_theme_mod('landingpress_header_alignment', 'center') ) {
			$site_branding_class .= ' site-header-align-'.$align;
		}
		$site_title_class = 'site-title clearfix';
		$header_logo = get_theme_mod( 'landingpress_header_logo' );
		if ( !$header_logo ) {
			$header_logo = get_template_directory_uri().'/assets/images/logo.png';
		}
		$header_image = get_header_image();
		$header_placement = get_theme_mod( 'landingpress_header_placement', 'background' );
		if ( $header_placement == 'background_nologo' ) {
			$header_placement = 'background';
			$site_title_class .= ' screen-reader-text';
		}
		if ( $header_image ) {
			$site_branding_class .= 'image' == $header_placement ? ' screen-reader-text' : '';
			$site_title_class .= 'image_desc_bottom' == $header_placement ? ' screen-reader-text' : '';
		}
		$site_branding_class .= $header_image ? ' site-header-image-active' : ' site-header-image-inactive';
		?>
		<?php if ( $header_image && ( 'image_title_bottom' == $header_placement || 'image_desc_bottom' == $header_placement ) ) : ?>
			<img class="site-header-image" src="<?php echo esc_url( $header_image ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
		<?php endif; ?>
		<div class="<?php echo esc_attr($site_branding_class); ?>">
			<div class="container">
				<div class="<?php echo esc_attr($site_title_class); ?>">
					<a class="header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
					</a>
					<?php if ( is_active_sidebar( 'header' ) ) : ?>
						<div class="header-widget">
							<?php dynamic_sidebar( 'header' ); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php if ( $header_image && ( 'image' == $header_placement || 'image_title_top' == $header_placement ) ) : ?>
			<img class="site-header-image" src="<?php echo esc_url( $header_image ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
		<?php endif; ?>
	</header>
<?php endif; ?>
<div class="site-inner">
	<?php if ( landingpress_is_menu_active() && has_nav_menu( 'header' ) ) : ?>
		<?php 
		$menu_logo = get_theme_mod( 'landingpress_menu_logo' );
		$menu_cart = class_exists( 'woocommerce' ) && get_theme_mod('landingpress_wc_minicart', '1') ? true : false;
		$menu_class = $menu_logo ? ' main-navigation-logo-yes' : ' main-navigation-logo-no';
		$menu_class .= $menu_cart ? ' main-navigation-cart-yes' : ' main-navigation-cart-no';
		?>
		<nav id="site-navigation" class="main-navigation <?php echo $menu_class; ?>">
			<div class="container">
				<div class="menu-overlay"></div>
				<button class="menu-toggle" aria-controls="header-menu" aria-expanded="false"><span class="menu-icon"><span class="menu-bar"></span><span class="menu-bar"></span><span class="menu-bar"></span></span></button>
				<?php if ( $menu_logo ) : ?>
					<a class="menu-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo esc_url( $menu_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
					</a>
				<?php endif; ?>
				<?php if ( $menu_cart ) : ?>
					<?php $wc_cart_url = function_exists('wc_get_cart_url') ? wc_get_cart_url() : WC()->cart->get_cart_url(); ?>
					<a class="menu-minicart" href="<?php echo esc_url( $wc_cart_url ); ?>">
						<i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="minicart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
					</a>
				<?php endif; ?>
				<?php 
				echo landingpress_get_nav_menu( array( 
					'theme_location' => 'header', 
					'container_class' => 'header-menu-container', 
					'menu_id' => 'header-menu', 
					'menu_class' => 'header-menu menu nav-menu clearfix', 
					'fallback_cb' => '',
				) ); 
				?>
			</div>
		</nav>
	<?php endif; ?>
	<div id="content" class="site-content">
		<div class="container">
			<?php do_action( 'landingpress_site_content_before' ); ?>
