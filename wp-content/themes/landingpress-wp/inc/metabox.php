<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_filter( 'cmb_meta_boxes', 'landingpress_cmb_meta_boxes' );
function landingpress_cmb_meta_boxes( array $meta_boxes ) {

	// _elementor_edit_mode = builder

	$fields = array(
		array( 
			'id' => '_landingpress_hide_sidebar',  
			'name' => esc_html__( 'Hide Sidebar', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_header',  
			'name' => esc_html__( 'Hide Header', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_menu',  
			'name' => esc_html__( 'Hide Header Menu', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_footerwidgets',  
			'name' => esc_html__( 'Hide Footer Widgets', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_footer',  
			'name' => esc_html__( 'Hide Footer', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_breadcrumb',  
			'name' => esc_html__( 'Hide Breadcrumb', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_title',  
			'name' => esc_html__( 'Hide Title', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_hide_comments',  
			'name' => esc_html__( 'Hide Comments', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'yes' => esc_html__( 'yes', 'landingpress-wp' ), 
			) 
		),
		array( 
			'id' => '_landingpress_page_width',  
			'name' => esc_html__( 'Page Width (Default Page Template)', 'landingpress-wp' ), 
			'type' => 'radio', 
			'options' => array( 
				'' => esc_html__( 'default', 'landingpress-wp' ), 
				'500' => '500px', 
				'600' => '600px', 
				'700' => '700px', 
				'800' => '800px'
			) 
		),
	);
	$meta_boxes[] = array(
		'id' => 'landingpress-layout',
		'title' => esc_html__( 'Page Layout Settings', 'landingpress-wp' ),
		'pages' => array( 'post', 'page' ),
		'fields' => $fields,
		'priority'   => 'low',
		'hide_on' => array( 
			'page-template' => array( 
				'page_landingpress.php', 
				'page_landingpress_boxed.php', 
				'page_landingpress_slim.php' 
			) 
		),
	);

	$fbevents = array(
		'PageView' => 'PageView '.esc_html__( '(default)', 'landingpress-wp' ),
		'ViewContent' => 'ViewContent',
		'AddToCart' => 'AddToCart',
		'InitiateCheckout' => 'InitiateCheckout',
		'AddPaymentInfo' => 'AddPaymentInfo',
		'Purchase' => 'Purchase',
		'AddToWishlist' => 'AddToWishlist',
		'Lead' => 'Lead',
		'CompleteRegistration' => 'CompleteRegistration',
		'custom' => 'Custom Event',
	);
	$fields = array(
		array( 
			'id' => '_landingpress_facebook-pixels', 
			'name' => esc_html__( 'Multiple Facebook Pixel IDs', 'landingpress-wp'), 
			'desc' => esc_html__( 'for this page only. If you need global multiple pixels for all pages, go to Appearance - Customize', 'landingpress-wp'), 
			'type' => 'group', 'cols' => 12, 
			'fields' => array(
				array( 
					'id' => 'pixel_id',  
					'name' => esc_html__( 'Facebook Pixel ID', 'landingpress-wp'), 
					'type' => 'text', 
					'cols' => 12 
				),
			), 
			'repeatable' => true, 
			'repeatable_max' => 10, 
			'sortable' => true, 
			'string-repeat-field' => esc_html__( 'Add Facebook Pixel ID', 'landingpress-wp'), 
			'string-delete-field' => esc_html__( 'Delete Facebook Pixel ID' , 'landingpress-wp'),
		),
		array( 
			'id' => '_landingpress_facebook-event',  
			'name' => esc_html__( 'Facebook Pixel Event', 'landingpress-wp'), 
			'type' => 'select', 
			'options' => $fbevents, 
		),
		array( 
			'id' => '_landingpress_facebook-custom-event', 
			'name' => esc_html__( 'Facebook Pixel Custom Event Name', 'landingpress-wp'), 
			'type' => 'text', 
			'cols' => 12, 
		),
		array( 
			'id' => '_landingpress_facebook-param-value', 
			'name' => 'value', 
			'type' => 'text', 
			'cols' => 6, 
		),
		array( 
			'id' => '_landingpress_facebook-param-currency', 
			'name' => 'currency', 
			'type' => 'select', 
			'options' => array(
				'IDR' => 'IDR',
				'USD' => 'USD',
			), 
			'cols' => 6, 
		),
		array( 
			'id' => '_landingpress_facebook-param-content_name', 
			'name' => 'content_name', 
			'type' => 'text', 
			'cols' => 4, 
		),
		array( 
			'id' => '_landingpress_facebook-param-content_ids', 
			'name' => 'content_ids', 
			'type' => 'text', 
			'cols' => 4, 
		),
		array( 
			'id' => '_landingpress_facebook-param-content_type', 
			'name' => 'content_type', 
			'type' => 'select', 
			'options' => array(
				'product' => 'product',
				'product_group' => 'product_group',
			), 
			'cols' => 4, 
		),
		array( 
			'id' => '_landingpress_facebook-custom-params', 
			'name' => esc_html__( 'Facebook Pixel Custom Parameters', 'landingpress-wp'), 
			'type' => 'group', 'cols' => 12, 
			'fields' => array(
				array( 
					'id' => 'custom_param_key',  
					'name' => esc_html__( 'Parameter Key', 'landingpress-wp'), 
					'type' => 'text', 
					'cols' => 6 
				),
				array( 
					'id' => 'custom_param_value',  
					'name' => esc_html__( 'Parameter Value', 'landingpress-wp'), 
					'type' => 'text', 
					'cols' => 6 
				),
			), 
			'repeatable' => true, 
			'repeatable_max' => 10, 
			'sortable' => true, 
			'string-repeat-field' => esc_html__( 'Add Custom Parameter', 'landingpress-wp'), 
			'string-delete-field' => esc_html__( 'Delete Custom Parameter' , 'landingpress-wp'),
		),
	);
	$meta_boxes[] = array(
		'id' => 'landingpress-facebook-pixel',
		'title' => esc_html__( 'Facebook Pixel Settings', 'landingpress-wp'),
		'pages' => array( 'post', 'page' ),
		'fields' => $fields,
		'priority'   => 'low',
	);

	// _yoast_wpseo_opengraph-title
	// _yoast_wpseo_opengraph-description
	// _yoast_wpseo_opengraph-image
	$fields = array(
		array( 
			'id' => '_landingpress_facebook-image', 
			'name' => esc_html__( 'Facebook Image', 'landingpress-wp'), 
			'type' => 'image' 
		),
		array( 
			'id' => '_landingpress_facebook-title', 
			'name' => esc_html__( 'Facebook Title', 'landingpress-wp'), 
			'type' => 'text' 
		),
		array( 
			'id' => '_landingpress_facebook-description', 
			'name' => esc_html__( 'Facebook Description', 'landingpress-wp'), 
			'type' => 'textarea' 
		),
	);
	$meta_boxes[] = array(
		'id' => 'landingpress-facebook-sharing',
		'title' => esc_html__( 'Facebook Sharing (Open Graph) Settings', 'landingpress-wp'),
		'pages' => array( 'post', 'page', 'product' ),
		'fields' => $fields,
		'priority'   => 'low',
	);

	if ( ! ( defined('WPSEO_VERSION') || class_exists('All_in_One_SEO_Pack') || class_exists('All_in_One_SEO_Pack_p') || class_exists('HeadSpace_Plugin') || class_exists('Platinum_SEO_Pack') || class_exists('SEO_Ultimate') ) ) {
		// _yoast_wpseo_meta-robots-noindex
		// _yoast_wpseo_meta-robots-nofollow
		// _yoast_wpseo_title
		// _yoast_wpseo_metadesc
		$fields = array(
			array( 
				'id' => '_landingpress_meta-title', 
				'name' => esc_html__( 'Meta Title', 'landingpress-wp'), 
				'type' => 'text' 
			),
			array( 
				'id' => '_landingpress_meta-description', 
				'name' => esc_html__( 'Meta Description', 'landingpress-wp'), 
				'type' => 'textarea' 
			),
			array( 
				'id' => '_landingpress_meta-keywords', 
				'name' => esc_html__( 'Meta Keywords', 'landingpress-wp'), 
				'type' => 'text' 
			),
			array( 
				'id' => '_landingpress_meta-index', 
				'name' => esc_html__( 'Meta Robots Index', 'landingpress-wp'), 
				'type' => 'select', 
				'options' => array( 
					'index' => 'index', 
					'noindex' => 'noindex' 
				), 
				'allow_none' => false, 
				'cols' => 6 
			),
			array( 
				'id' => '_landingpress_meta-follow', 
				'name' => esc_html__( 'Meta Robots Follow', 'landingpress-wp'), 
				'type' => 'select', 
				'options' => array( 
					'follow' => 'follow', 
					'nofollow' => 'nofollow' 
				), 
				'allow_none' => false, 
				'cols' => 6 
			),
		);
		$meta_boxes[] = array(
			'id' => 'landingpress-seo',
			'title' => esc_html__( 'On-Page SEO Settings', 'landingpress-wp'),
			'pages' => array( 'post', 'page', 'product' ),
			'fields' => $fields,
			'priority'   => 'low',
		);
	}

	$fields = array(
		array( 
			'id' => '_landingpress_header_script', 
			'name' => esc_html__( 'Custom Header Script', 'landingpress-wp'), 
			'type' => 'textarea' 
		),
		array( 
			'id' => '_landingpress_footer_script', 
			'name' => esc_html__( 'Custom Footer Script', 'landingpress-wp'), 
			'type' => 'textarea' 
		),
	);
	$meta_boxes[] = array(
		'id' => 'landingpress-scripts',
		'title' => esc_html__( 'Header and Footer Scripts', 'landingpress-wp'),
		'pages' => array( 'post', 'page' ),
		'fields' => $fields,
		'priority'   => 'low',
	);

	$fields = array(
		array( 
			'id' => '_landingpress_redirect', 
			'name' => esc_html__( 'Redirect URL', 'landingpress-wp'), 
			'type' => 'text' 
		),
	);
	$meta_boxes[] = array(
		'id' => 'landingpress-redirect',
		'title' => esc_html__( 'Redirect Settings', 'landingpress-wp'),
		'pages' => array( 'post', 'page', 'product' ),
		'fields' => $fields,
		'priority'   => 'low',
	);

	return $meta_boxes;

}

add_action( 'admin_head-post.php', 'landingpress_cmb_meta_boxes_scripts' );
add_action( 'admin_head-post-new.php', 'landingpress_cmb_meta_boxes_scripts' );
function landingpress_cmb_meta_boxes_scripts() {
    ?>
	<script type="text/javascript">
	/*<![CDATA[*/
	jQuery(document).ready(function($){
		if ( $('#_landingpress_facebook-event').length ) {
			var lp_fb_event = $('#_landingpress_facebook-event select').val();
			// console.log( 'lp_fb_event = ' + lp_fb_event );
			if ( lp_fb_event == 'custom' ) {
				$('#_landingpress_facebook-custom-event').show();
			}
			else {
				$('#_landingpress_facebook-custom-event').hide();
			}
			if ( lp_fb_event != '' && lp_fb_event != 'PageView' && lp_fb_event != 'custom' ) {
				$('#_landingpress_facebook-param-value').show();
				$('#_landingpress_facebook-param-currency').show();
				$('#_landingpress_facebook-param-content_name').show();
			}
			else {
				$('#_landingpress_facebook-param-value').hide();
				$('#_landingpress_facebook-param-currency').hide();
				$('#_landingpress_facebook-param-content_name').hide();
			}
			if ( lp_fb_event != '' && lp_fb_event != 'PageView' && lp_fb_event != 'custom' && lp_fb_event != 'Lead' && lp_fb_event != 'CompleteRegistration' ) {
				$('#_landingpress_facebook-param-content_ids').show();
				$('#_landingpress_facebook-param-content_type').show();
			}
			else {
				$('#_landingpress_facebook-param-content_ids').hide();
				$('#_landingpress_facebook-param-content_type').hide();
			}
			if ( lp_fb_event != '' && lp_fb_event != 'PageView' ) {
				$('#_landingpress_facebook-custom-params').show();
			}
			else {
				$('#_landingpress_facebook-custom-params').hide();
			}
			$(document).on('change', '#_landingpress_facebook-event select', function() {
				lp_fb_event = $(this).find('option:selected').val();
				// console.log( 'lp_fb_event = ' + lp_fb_event );
				if ( lp_fb_event == 'custom' ) {
					$('#_landingpress_facebook-custom-event').show();
				}
				else {
					$('#_landingpress_facebook-custom-event').hide();
				}
				if ( lp_fb_event != '' && lp_fb_event != 'PageView' && lp_fb_event != 'custom' ) {
					$('#_landingpress_facebook-param-value').show();
					$('#_landingpress_facebook-param-currency').show();
					$('#_landingpress_facebook-param-content_name').show();
				}
				else {
					$('#_landingpress_facebook-param-value').hide();
					$('#_landingpress_facebook-param-currency').hide();
					$('#_landingpress_facebook-param-content_name').hide();
				}
				if ( lp_fb_event != '' && lp_fb_event != 'PageView' && lp_fb_event != 'custom' && lp_fb_event != 'Lead' && lp_fb_event != 'CompleteRegistration' ) {
					$('#_landingpress_facebook-param-content_ids').show();
					$('#_landingpress_facebook-param-content_type').show();
				}
				else {
					$('#_landingpress_facebook-param-content_ids').hide();
					$('#_landingpress_facebook-param-content_type').hide();
				}
				if ( lp_fb_event != '' && lp_fb_event != 'PageView' ) {
					$('#_landingpress_facebook-custom-params').show();
				}
				else {
					$('#_landingpress_facebook-custom-params').hide();
				}
			});
		}
	});
	/*]]>*/
	</script>
	<?php 
}
