<?php
use FDC\Core\Fdc_Helper as fdcHelper;

if( ! function_exists('fdc_get_total_number_generate') ){
/**
 * Function that adds all the generated objects
 *
 * @since   1.1     2019-10-30      Release
 * @since   1.5     2019-11-11      - Calculate for each object the number of seconds saved by the user
 *                                  - Separate time and object number through locations
 * @since   1.6     2019-11-25      Menus counter added
 *
 * @return  array
 */
function fdc_get_total_number_generate (){
    $seconds = 0;
    $data_totals = fdcHelper::get_option( 'fdc-setting' );
    $seconds += 5 * (int)$data_totals->users ; // x second per user
    $seconds += 10 * (int)$data_totals->comments ; // x second per comment
    $seconds += 120 * (int)$data_totals->posts ; // x second per post
    $seconds += 5 * (int)$data_totals->terms ; // x second per term
    $seconds += 5 * (int)$data_totals->menus ; // x second per menu

    $result = [
        'time'   => $seconds,
        'number' => (int)$data_totals->users + (int)$data_totals->comments + (int)$data_totals->posts + (int)$data_totals->terms,
        'objects'=> [
            'users'    => (int)$data_totals->users,
            'comments' => (int)$data_totals->comments,
            'posts'    => (int)$data_totals->posts,
            'terms'    => (int)$data_totals->terms,
            'menus'    => (int)$data_totals->menus,
        ]
    ];

    return $result;
}
}