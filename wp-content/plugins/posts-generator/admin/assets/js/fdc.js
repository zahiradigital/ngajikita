/*
|--------------------------------------------------------------------------
| Pixel Framework Scripts
|--------------------------------------------------------------------------
|
| Libraries and functions that allow the correct functioning of Pixel.
| All based on the backend.
|
*/
;(function( $, window, document, undefined ) {
  'use strict';

  // --- Constants ---------------
  var FDC = FDC || {};

  FDC.vars = {
    onloaded   : false,
    $body      : $('body'),
    $window    : $(window),
    $document  : $(document),
    is_rtl     : $('body').hasClass('rtl'),
  };

  FDC.funcs = {};

  // --- Helpers ---------------
  FDC.helper = {

    // uID user
    uid: function ( prefix ) {
      return ( prefix || '' ) + Math.random().toString(36).substr(2, 9);
    },

    // Quote regular expression characters
    preg_quote: function( str ) {
      return (str+'').replace(/(\[|\-|\])/g, "\\$1");
    },

    // Get a cookie
    get_cookie: function( name ) {

      var e, b, cookie = document.cookie, p = name + '=';
      if( ! cookie ) {
        return;
      }

      b = cookie.indexOf( '; ' + p );
      if( b === -1 ) {
        b = cookie.indexOf(p);
        if( b !== 0 ) {
          return null;
        }
      } else {
        b += 2;
      }

      e = cookie.indexOf( ';', b );
      if( e === -1 ) {
        e = cookie.length;
      }

      return decodeURIComponent( cookie.substring( b + p.length, e ) );
    },

    // Set cookie
    set_cookie: function( name, value, expires, path, domain, secure ) {

      var d = new Date();

      if( typeof( expires ) === 'object' && expires.toGMTString ) {
        expires = expires.toGMTString();
      } else if( parseInt( expires, 10 ) ) {
        d.setTime( d.getTime() + ( parseInt( expires, 10 ) * 1000 ) );
        expires = d.toGMTString();
      } else {
        expires = '';
      }

      document.cookie = name + '=' + encodeURIComponent( value ) +
        ( expires ? '; expires=' + expires : '' ) +
        ( path    ? '; path=' + path       : '' ) +
        ( domain  ? '; domain=' + domain   : '' ) +
        ( secure  ? '; secure'             : '' );

    },

    // Remove a cookie
    remove_cookie: function( name, path, domain, secure ) {
      FDC.helper.set_cookie( name, '', -1000, path, domain, secure );
    },

  }

  // --- Handle loading overlays ---------------
  var fdc_loader = {
    /**
     * Initialize our loading overlays for use
     * @params void
     * @return void
     */
    initialize : function () {
      var html =
        '<div class="loading-overlay">' +
        '<div class="loading-overlay-image-container">' +
          '<img src="'+fdc_vars.url+'admin/assets/images/loader.svg" class="loading-overlay-img" />' +
        '</div></div>';

      // append our html to the DOM body
      $( 'body .pf-fdc-setting .pf-container' ).append( html );
    },

    /**
     * Show the loading overlay
     * @params void
     * @return void
     */
    showLoader : function () {
      jQuery( '.loading-overlay' ).show();
      jQuery( '.loading-overlay-image-container' ).show();
    },

    /**
     * Hide the loading overlay
     * @params void
     * @return void
     */
    hideLoader : function () {
      jQuery( '.loading-overlay' ).remove();
      jQuery( '.loading-overlay-image-container' ).remove();
    }
  }

  // --- Valid error when generating (at the moment in the menu) ---------------
  var fdc_error = {

    load : function (){
      $('.pf-fdc-setting .class-menus .pf-field-notice > div').css('display','none');
      $('.pf-fdc-setting .class-menus .pf-label-error').css('display','none');
    },

    validate_menus : function ( location ) {
      var selected = '';
      $('.pf-fdc-setting .class-menus .pf-field-notice').addClass('hidden');
      $('.pf-fdc-setting .class-menus .class-location input:checked').each ( function() {
        selected += $(this).val() + ",";
      }); selected = selected.slice(0, -1);

      if( ! selected ){
        $('.pf-fdc-setting .class-menus .pf-field-notice').removeClass('hidden');
        $('.pf-fdc-setting .class-menus .pf-field-notice > div').css('display','block');
        $('.pf-fdc-setting .class-menus .pf-label-error').css('display','inline-block');
        $('html, body').animate({ scrollTop: 0 }, 'slow');
        return true;
      }else{
        $('.pf-fdc-setting .class-menus .pf-field-notice').addClass('hidden');
        $('.pf-fdc-setting .class-menus .pf-field-notice > div').css('display','none');
        $('.pf-fdc-setting .class-menus .pf-label-error').css('display','none');
        return false;
      }
    }
  }


  var fdc_result_out = function( object, classs, data, object_name ){

    var $wrap    = object.closest('.pf-accordion-content'),
        class_msg= data.refresh == 0 ? 'pf-submessage-success' : 'pf-submessage-danger',
        $content = $wrap.find('.fdc_output');
        var html =
            '<div class="pf-field pf-field-submessage">' +
            '<div class="pf-submessage '+ class_msg +'">';

        if( data ){
          if( data.refresh != 1 && ! data.mesagge ){
            html = html + '<div><strong>Stats:</strong> ' + data.results.length + ' ' + object_name + ' in ' + data.time + '</div>';
            html = html + '<div><strong>Results:</strong> ';
            for(var k in data.results) {
              var o = data.results[k];
              var slink = '';
              if( o.link ){
                slink = '<a href="'+ o.link +'" target="_blank"><i class="fa fa-external-link" aria-hidden="true"></i></a>&nbsp;&nbsp;';
              }
              html = html + '<a target="_blank" href="'+ o.url +'">' + o.id + '</a> '+ slink +', ';
            }
            html = html + '</div>';
          }else if( data.refresh != 1 && data.mesagge ){
            html = html + '<div><strong>Stats:</strong> ' + data.mesagge + ' in ' + data.time + '</div>';
          }else{
            html = html + "ERROR: Please refresh the page";
          }
        }
        html = html.slice(0, -2);
        html = html + '</div></div>';

        // Add to the counter
        $("#footer-upgrade .fdc-number-obj > strong").text( parseInt($("#footer-upgrade .fdc-number-obj > strong").text()) + data.results.length );

        // Show the result
        $content.append( html );
  }

  // --- Exact or random field ---------------
  var fdc_active_exact_or_random = function() {
    $('.fdc_extact_or_random').each( function() {
      var $this           = $(this),
          $control_switch = $this.find('.pf-field-switcher'),
          $input_spint    = $this.find('.pf-field-spinner.fdc_exact_input_spinner'),
          $input_spint2   = $this.find('.pf-field-spinner.fdc_random_input_spinner');

          $control_switch.find('.pf-fieldset').on('click', function(){
            var $value = $(this).find('input').val();
            if( $value == 1 ){
              $input_spint2.addClass('hidden');
              $input_spint.removeClass('hidden');
            }else{
              $input_spint.addClass('hidden');
              $input_spint2.removeClass('hidden');
            }
          });
    });
  };

  // --- Input meta name fix ---------------
  var fdc_input_fix_meta_name_space = function() {
    $('.class-meta .class-meta_name input').each( function() {
      $(document).on('input', '._fdc_meta_input_name', function() {
      //$(this).on('input', function(key) {
        var value = $(this).val();
        $(this).val(value.replace(/ /g, '_'));
      });
    });
  }

  /*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 *
 * @link http://blog.stevenlevithan.com/archives/date-time-format
 */

  var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
      timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
      timezoneClip = /[^-+\dA-Z]/g,
      pad = function (val, len) {
        val = String(val);
        len = len || 2;
        while (val.length < len) val = "0" + val;
        return val;
      };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
      var dF = dateFormat;

      // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
      if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
        mask = date;
        date = undefined;
      }

      // Passing date through Date applies Date.parse, if necessary
      date = date ? new Date(date) : new Date;
      if (isNaN(date)) throw SyntaxError("invalid date");

      mask = String(dF.masks[mask] || mask || dF.masks["default"]);

      // Allow setting the utc argument via the mask
      if (mask.slice(0, 4) == "UTC:") {
        mask = mask.slice(4);
        utc = true;
      }

      var	_ = utc ? "getUTC" : "get",
        d = date[_ + "Date"](),
        D = date[_ + "Day"](),
        m = date[_ + "Month"](),
        y = date[_ + "FullYear"](),
        H = date[_ + "Hours"](),
        M = date[_ + "Minutes"](),
        s = date[_ + "Seconds"](),
        L = date[_ + "Milliseconds"](),
        o = utc ? 0 : date.getTimezoneOffset(),
        flags = {
          d:    d,
          dd:   pad(d),
          ddd:  dF.i18n.dayNames[D],
          dddd: dF.i18n.dayNames[D + 7],
          m:    m + 1,
          mm:   pad(m + 1),
          mmm:  dF.i18n.monthNames[m],
          mmmm: dF.i18n.monthNames[m + 12],
          yy:   String(y).slice(2),
          yyyy: y,
          h:    H % 12 || 12,
          hh:   pad(H % 12 || 12),
          H:    H,
          HH:   pad(H),
          M:    M,
          MM:   pad(M),
          s:    s,
          ss:   pad(s),
          l:    pad(L, 3),
          L:    pad(L > 99 ? Math.round(L / 10) : L),
          t:    H < 12 ? "a"  : "p",
          tt:   H < 12 ? "am" : "pm",
          T:    H < 12 ? "A"  : "P",
          TT:   H < 12 ? "AM" : "PM",
          Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
          o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
          S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
        };

      return mask.replace(token, function ($0) {
        return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
      });
    };
  }();

  // Some common format strings
  dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
  };

  // Internationalization strings
  dateFormat.i18n = {
    dayNames: [
      "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
      "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
      "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
      "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
  };

  // For convenience...
  Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
  };



  // https://stackoverflow.com/questions/11127227/jquery-serialize-input-with-arrays
  $.fn.serializeControls2 = function() {
    var data = {};

    function buildInputObject(arr, val) {
        if (arr.length < 1) {
            return val;
        }
        var objkey = arr[0];
        if (objkey.slice(-1) == "]") {
            objkey = objkey.slice(0,-1);
        }
        var result = {};
        if (arr.length == 1){
            result[objkey] = val;
        } else {
            arr.shift();
            var nestedVal = buildInputObject(arr,val);
            result[objkey] = nestedVal;
        }
        return result;
    }

    function gatherMultipleValues( that ) {
        var final_array = [];
        $.each(that.serializeArray(), function( key, field ) {
            // Copy normal fields to final array without changes
            if( field.name.indexOf('[]') < 0 ){
                final_array.push( field );
                return true; // That's it, jump to next iteration
            }

            // Remove "[]" from the field name
            var field_name = field.name.split('[]')[0];

            // Add the field value in its array of values
            var has_value = false;
            $.each( final_array, function( final_key, final_field ){
                if( final_field.name === field_name ) {
                    has_value = true;
                    final_array[ final_key ][ 'value' ].push( field.value );
                }
            });
            // If it doesn't exist yet, create the field's array of values
            if( ! has_value ) {
                final_array.push( { 'name': field_name, 'value': [ field.value ] } );
            }
        });
        return final_array;
    }

    // Manage fields allowing multiple values first (they contain "[]" in their name)
    var final_array = gatherMultipleValues( this );

    // Then, create the object
    $.each(final_array, function() {
        var val = this.value;
        var c = this.name.split('[');
        var a = buildInputObject(c, val);
        $.extend(true, data, a);
    });

    return data;
  };

  // --- Date calculate range ---------------
  $.fn.fdc_set_range_date = function() {
    this.each( function() {
      var $wrap = $(this),
          $select = $wrap.find('select'),
          $input_from = $wrap.find('.pf-field-date.class-date_from input[type="text"]'),
          $input_to = $wrap.find('.pf-field-date.class-date_to > input[type="text"]'),

          myDateFormat = $input_from.attr('custom-date-format');

          $select.on('change', function() {
            switch ( $(this).val() ) {
              case 'today':
                $input_from.val(moment().format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'tomorrow':
                $input_from.val(moment().add(1, 'days').format(myDateFormat));
                $input_to.val(moment().add(1, 'days').format(myDateFormat));
                break;
              case 'yesterday':
                $input_from.val(moment().add(-1, 'days').format(myDateFormat));
                $input_to.val(moment().add(-1, 'days').format(myDateFormat));
                break;
              case 'this-week':
                $input_from.val(moment().startOf('isoWeek').format(myDateFormat));
                $input_to.val(moment().endOf('isoWeek').format(myDateFormat));
                break;
              case 'this-month':
                $input_from.val(moment().startOf('month').format(myDateFormat));
                $input_to.val(moment().endOf('month').format(myDateFormat));
                break;
              case 'this-year':
                $input_from.val(moment().startOf('year').format(myDateFormat));
                $input_to.val(moment().endOf('year').format(myDateFormat));
                break;
              case 'last-7':
                $input_from.val(moment().add(-7, 'days').format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'last-15':
                $input_from.val(moment().add(-15, 'days').format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'last-30':
                $input_from.val(moment().add(-30, 'days').format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'last-60':
                $input_from.val(moment().add(-60, 'days').format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'last-year':
                $input_from.val(moment().add(-365, 'days').format(myDateFormat));
                $input_to.val(moment().format(myDateFormat));
                break;
              case 'next-15':
                $input_from.val(moment().format(myDateFormat));
                $input_to.val(moment().add(15, 'days').format(myDateFormat));
                break;
              case 'next-30':
                $input_from.val(moment().format(myDateFormat));
                $input_to.val(moment().add(30, 'days').format(myDateFormat));
                break;
              case 'next-60':
                $input_from.val(moment().format(myDateFormat));
                $input_to.val(moment().add(60, 'days').format(myDateFormat));
                break;
            };
          });
    });
  }

  // --- Generate users ---------------
  $.fn.generate_users = function() {

    return this.each( function() {
      $(this).on("click",function(){

        fdc_loader.initialize();
        fdc_loader.showLoader();

        var $button = $(this);
        var chkId = '', tags = '';
        $('.pf-fdc-setting .class-users .class-role input:checked').each ( function() {
          chkId += $(this).val() + ",";
        });
        chkId = chkId.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-users .class-description_tag input:checked').each ( function() {
          tags += $(this).val() + ",";
        });
        tags = tags.slice(0, -1);

        window.wp.ajax.post( 'generate-users', {
          'nonce'          : $(this).attr("data-nonce"),

          'type' : $('.pf-fdc-setting input[name="fdc-setting[users][quantity][type]"]').val(),
          'exact': $('.pf-fdc-setting input[name="fdc-setting[users][quantity][num_exact]"]').val(),
          'from' : $('.pf-fdc-setting input[name="fdc-setting[users][quantity][from]"]').val(),
          'to'   : $('.pf-fdc-setting input[name="fdc-setting[users][quantity][to]"]').val(),
          'roles': chkId.split(","),

          'quantities_type_paragraph': $('.pf-fdc-setting input[name="fdc-setting[users][paragraph_quantity][type]"]').val(),
          'exact_paragraph'          : $('.pf-fdc-setting input[name="fdc-setting[users][paragraph_quantity][num_exact]"]').val(),
          'from_paragraph'           : $('.pf-fdc-setting input[name="fdc-setting[users][paragraph_quantity][from]"]').val(),
          'to_paragraph'             : $('.pf-fdc-setting input[name="fdc-setting[users][paragraph_quantity][to]"]').val(),
          'length_paragraph'         : $('.pf-fdc-setting select[name="fdc-setting[users][description_length]"]').val(),
          'tags'                     : tags.split(","),

          'metas': $(".class-users .class-meta").find("select, textarea, input, input:checked, textarea.tag-editor").serializeControls2(),
        })
        .done( function( response ) {
          console.log( response );
          fdc_result_out( $button, '', response, 'users' );
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });

      });
    });
  }

  // --- Generate Terms ---------------
  $.fn.generate_terms = function() {
    return this.each( function() {
      $(this).on("click",function(){

        fdc_loader.initialize();
        fdc_loader.showLoader();

        var $button = $(this);
        var chkId = '', tags = '';
        $('.pf-fdc-setting .class-terms .class-taxonomies input:checked').each ( function() {
          chkId += $(this).val() + ",";
        }); chkId = chkId.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-terms .class-description_tag input:checked').each ( function() {
          tags += $(this).val() + ",";
        }); tags = tags.slice(0, -1);

        window.wp.ajax.post( 'generate-terms', {
          'nonce'          : $(this).attr("data-nonce"),

          'type' : $('.pf-fdc-setting input[name="fdc-setting[terms][quantity][type]"]').val(),
          'exact': $('.pf-fdc-setting input[name="fdc-setting[terms][quantity][num_exact]"]').val(),
          'from' : $('.pf-fdc-setting input[name="fdc-setting[terms][quantity][from]"]').val(),
          'to'   : $('.pf-fdc-setting input[name="fdc-setting[terms][quantity][to]"]').val(),
          'taxonomies': chkId.split(","),

          'from_words': $('.pf-fdc-setting input[name="fdc-setting[terms][size_word][from]"]').val(),
          'to_words'  : $('.pf-fdc-setting input[name="fdc-setting[terms][size_word][to]"]').val(),

          'quantities_type_paragraph': $('.pf-fdc-setting input[name="fdc-setting[terms][paragraph_quantity][type]"]').val(),
          'exact_paragraph'          : $('.pf-fdc-setting input[name="fdc-setting[terms][paragraph_quantity][num_exact]"]').val(),
          'from_paragraph'           : $('.pf-fdc-setting input[name="fdc-setting[terms][paragraph_quantity][from]"]').val(),
          'to_paragraph'             : $('.pf-fdc-setting input[name="fdc-setting[terms][paragraph_quantity][to]"]').val(),
          'length_paragraph'         : $('.pf-fdc-setting select[name="fdc-setting[terms][description_length]"]').val(),
          'tags'                     : tags.split(","),

          'metas': $(".class-terms .class-meta").find("select, textarea, input, input:checked, textarea.tag-editor").serializeControls2(),
        })
        .done( function( response ) {
          console.log( response );
          fdc_result_out( $button, '', response, 'terms' );
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });

      });
    });
  }

  // --- Generate Posts ---------------
  $.fn.generate_posts = function() {
    return this.each( function() {
      $(this).on("click",function(){

        var $button = $(this);

        fdc_loader.initialize();
        fdc_loader.showLoader();

        var tags           = '',
            tags_block     = '',
            post_status    = '',
            comment_status = '',
            post_type      = '',
            tax_cate       = '',
            tax_tag        = '',
            users          = '',
            image_f_source = '',
            image_p_source = '',
            image_p_banner = '',
            socials        = '';
        //-
        $('.pf-fdc-setting .class-posts .class-description_tag input:checked').each ( function() { tags += $(this).val() + ","; });
        tags = tags.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-description_tag_block input:checked').each ( function() { tags_block += $(this).val() + ","; });
        tags_block = tags_block.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-comment_status input:checked').each ( function() { comment_status += $(this).val() + ","; });
        comment_status = comment_status.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-posts_status input:checked').each ( function() { post_status += $(this).val() + ","; });
        post_status = post_status.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-post_type input:checked').each ( function() { post_type += $(this).val() + ","; });
        post_type = post_type.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-taxonomies_cate input:checked').each ( function() { tax_cate += $(this).val() + ",";});
        tax_cate = tax_cate.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .pf-taxonomy-content-no-hierarchical textarea').each ( function() { tax_tag += $(this).val() + ","; } );
        tax_tag = tax_tag.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts select[name="fdc-setting[posts][users][]"]').find(':selected').each ( function() { users += $(this).val() + ","; } );
        users = users.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-source2 input:checked').each ( function() { image_f_source += $(this).val() + ","; });
        image_f_source = image_f_source.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-source input:checked').each ( function() { image_p_source += $(this).val() + ","; });
        image_p_source = image_p_source.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-banner input:checked').each ( function() { image_p_banner += $(this).val() + ","; });
        image_p_banner = image_p_banner.slice(0, -1);
        //-
        $('.pf-fdc-setting .class-posts .class-social_embed input:checked').each ( function() { socials += $(this).val() + ","; });
        socials = socials.slice(0, -1);

        var data = {
          'nonce'          : $(this).attr("data-nonce"),

          // Number of posts
          'type' : $('.pf-fdc-setting input[name="fdc-setting[posts][quantity][type]"]').val(),
          'exact': $('.pf-fdc-setting input[name="fdc-setting[posts][quantity][num_exact]"]').val(),
          'from' : $('.pf-fdc-setting input[name="fdc-setting[posts][quantity][from]"]').val(),
          'to'   : $('.pf-fdc-setting input[name="fdc-setting[posts][quantity][to]"]').val(),

          // Title
          'title_from'  : $('.pf-fdc-setting input[name="fdc-setting[posts][title_quantity][from]"]').val(),
          'title_to'    : $('.pf-fdc-setting input[name="fdc-setting[posts][title_quantity][to]"]').val(),
          'title_length': $('.pf-fdc-setting input[name="fdc-setting[posts][title_size][width]"]').val(),

          // Content
          'content_type'     : $('.pf-fdc-setting input[name="fdc-setting[posts][paragraph_quantity][type]"]').val(),
          'content_exact'    : $('.pf-fdc-setting input[name="fdc-setting[posts][paragraph_quantity][num_exact]"]').val(),
          'content_from'     : $('.pf-fdc-setting input[name="fdc-setting[posts][paragraph_quantity][from]"]').val(),
          'content_to'       : $('.pf-fdc-setting input[name="fdc-setting[posts][paragraph_quantity][to]"]').val(),
          'content_length'   : $('.pf-fdc-setting select[name="fdc-setting[posts][description_length]"]').val(),
          'content_tag'      : tags,
          'content_tag_block': tags_block,
          'content_social'   : socials,

          // Image
          'imagen_type'          : $('.pf-fdc-setting select[name="fdc-setting[posts][media_type]"]').val(),
          'image_featured_source': image_f_source,
          'image_featured_width' : $('.pf-fdc-setting input[name="fdc-setting[posts][media_featured_image][size][width]"]').val(),
          'image_featured_height': $('.pf-fdc-setting input[name="fdc-setting[posts][media_featured_image][size][height]"]').val(),

          'image_post_source': image_p_source,
          'image_post_width' : $('.pf-fdc-setting input[name="fdc-setting[posts][media_post_image][size][width]"]').val(),
          'image_post_height': $('.pf-fdc-setting input[name="fdc-setting[posts][media_post_image][size][height]"]').val(),
          'image_post_banner': image_p_banner,
          'image_post_from'  : $('.pf-fdc-setting input[name="fdc-setting[posts][media_post_image][quantity][from]"]').val(),
          'image_post_to'    : $('.pf-fdc-setting input[name="fdc-setting[posts][media_post_image][quantity][to]"]').val(),

          // Setting
          'users'         : users,
          'post_status'   : post_status,
          'comment_status': comment_status,
          'post_type'     : post_type,
          'date_from'     : $('.pf-fdc-setting input[name="fdc-setting[posts][date][date_from]"]').val(),
          'date_to'       : $('.pf-fdc-setting input[name="fdc-setting[posts][date][date_to]"]').val(),

          // Taxonomy
          'tax_type': $('.pf-fdc-setting input[name="fdc-setting[posts][taxonomies_type]"]:checked').val(),
          'tax_cate': tax_cate,
          'tax_tag' : tax_tag,
          'tax_from': $('.pf-fdc-setting input[name="fdc-setting[posts][quantity_term][from]"]').val(),
          'tax_to'  : $('.pf-fdc-setting input[name="fdc-setting[posts][quantity_term][to]"]').val(),


          'metas': $(".class-posts .class-meta").find("select, textarea, input, input:checked, textarea.tag-editor").serializeControls2(),
        };
        console.log('data: ', data);

        window.wp.ajax.post( 'generate-posts', data)
        .done( function( response ) {
          console.log( response );
          fdc_result_out( $button, '', response, 'posts' );
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });

      });
    });
  }

  // --- Generate Comments ---------------
  $.fn.generate_comments = function() {
    return this.each( function() {
      $(this).on("click",function(){

        fdc_loader.initialize();
        fdc_loader.showLoader();

        var $button = $(this);
        var chkId = '', tags = '';
        $('.pf-fdc-setting .class-comments .class-post_type input:checked').each ( function() {
          chkId += $(this).val() + ",";
        });
        //-
        $('.pf-fdc-setting .class-comments .class-description_tag input:checked').each ( function() {
          tags += $(this).val() + ",";
        });
        tags = tags.slice(0, -1);

        var data = {
          'nonce'          : $(this).attr("data-nonce"),

          'type'     : $('.pf-fdc-setting input[name="fdc-setting[comments][quantity][type]"]').val(),
          'exact'    : $('.pf-fdc-setting input[name="fdc-setting[comments][quantity][num_exact]"]').val(),
          'from'     : $('.pf-fdc-setting input[name="fdc-setting[comments][quantity][from]"]').val(),
          'to'       : $('.pf-fdc-setting input[name="fdc-setting[comments][quantity][to]"]').val(),
          'post_type': chkId.split(","),

          'quantities_type_paragraph': $('.pf-fdc-setting input[name="fdc-setting[comments][paragraph_quantity][type]"]').val(),
          'exact_paragraph'          : $('.pf-fdc-setting input[name="fdc-setting[comments][paragraph_quantity][num_exact]"]').val(),
          'from_paragraph'           : $('.pf-fdc-setting input[name="fdc-setting[comments][paragraph_quantity][from]"]').val(),
          'to_paragraph'             : $('.pf-fdc-setting input[name="fdc-setting[comments][paragraph_quantity][to]"]').val(),
          'length_paragraph'         : $('.pf-fdc-setting select[name="fdc-setting[comments][description_length]"]').val(),
          'tags'                     : tags.split(","),

          'metas': $(".class-comments .class-meta").find("select, textarea, input, input:checked, textarea.tag-editor").serializeControls2(),
        };
        console.log('data: ', data);

        window.wp.ajax.post( 'generate-comments', data)
        .done( function( response ) {
          console.log( response );
          fdc_result_out( $button, '', response, 'comments' );
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });

      });
    });
  }

  // --- Generate Menus ---------------
  $.fn.generate_menus = function() {
    return this.each( function() {
      $(this).on("click",function(){

        fdc_error.load();
        if( fdc_error.validate_menus() ) return;

        fdc_loader.initialize();
        fdc_loader.showLoader();

        var $button = $(this);
        var chkId = '', tags = '';
        $('.pf-fdc-setting .class-menus .class-location input:checked').each ( function() {
          chkId += $(this).val() + ",";
        });
        chkId = chkId.slice(0, -1);

        var data = {
          'nonce'          : $(this).attr("data-nonce"),

          'type' : $('.pf-fdc-setting input[name="fdc-setting[menus][config_single_level][quantity][type]"]').val(),
          'exact': $('.pf-fdc-setting input[name="fdc-setting[menus][config_single_level][quantity][num_exact]"]').val(),
          'from' : $('.pf-fdc-setting input[name="fdc-setting[menus][config_single_level][quantity][from]"]').val(),
          'to'   : $('.pf-fdc-setting input[name="fdc-setting[menus][config_single_level][quantity][to]"]').val(),

          'm_type' : $('.pf-fdc-setting input[name="fdc-setting[menus][config_multilevel][quantity][type]"]').val(),
          'm_exact': $('.pf-fdc-setting input[name="fdc-setting[menus][config_multilevel][quantity][num_exact]"]').val(),
          'm_from' : $('.pf-fdc-setting input[name="fdc-setting[menus][config_multilevel][quantity][from]"]').val(),
          'm_to'   : $('.pf-fdc-setting input[name="fdc-setting[menus][config_multilevel][quantity][to]"]').val(),

          'location': chkId.split(","),
          'level'   : $('.pf-fdc-setting select[name="fdc-setting[menus][nivel]"]').val(),

          //'metas': $(".class-menus .class-meta").find("select, textarea, input, input:checked, textarea.tag-editor").serializeControls2(),
        };
        console.log('data: ', data);

        window.wp.ajax.post( 'generate-menus', data)
        .done( function( response ) {
          console.log( response );
          fdc_result_out( $button, '', response, 'menus' );
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });

      });
    });
  }

  // --- Delete! ---------------
  $.fn.generate_delete = function() {
    return this.each( function() {
      $(this).on("click",function(){

        fdc_loader.initialize();
        fdc_loader.showLoader();

        // --- Validate delete all data ---------------
        var $input_validate  = $('.pf-fdc-setting input[name="fdc-setting[delete][text_verification]"]');
        var $select_validate = $('.pf-fdc-setting select[name="fdc-setting[delete][delete_type]"]');
        $input_validate.removeClass('fdc-input-alert');
        if( $select_validate.val() == 'all' ){
          if( $input_validate.val() != 'I want to delete all the data' ){
            $input_validate.addClass('fdc-input-alert');
            fdc_loader.hideLoader();
            $input_validate.focus();
            return;
          }
        }

        var object_string = '';
        $('.pf-fdc-setting .class-delete .class-delete_object input:checked').each ( function() {
          object_string += $(this).val() + ",";
        });
        object_string = object_string.slice(0, -1);

        var data = {
          'nonce'  : $(this).attr("data-nonce"),
          'objects': object_string,
          'type'   : $('.pf-fdc-setting select[name="fdc-setting[delete][delete_type]"]').val(),
        }
        console.log('data: ', data);

        window.wp.ajax.post( 'generate-delete-data', data)
        .done( function( response ) {
          console.log( response );
          $input_validate.val('');
          fdc_loader.hideLoader();
        })
        .fail( function( response ) {
          console.log( response );
          fdc_loader.hideLoader();
        });
      });
    });
  };

  $.fn.refresh_validate_name_meta = function(){
    $(this).on("click", function(){
      fdc_input_fix_meta_name_space();
    });
  }


  // --- Document ready and run scripts ---------------
  $(document).ready( function() {

    // Reset errors
    fdc_error.load();

    // Generate users
    $('.pf-fdc-setting .fdc-button-users').generate_users();

    // Generate terms
    $('.pf-fdc-setting .fdc-button-terms').generate_terms();

    // Generate posts
    $('.pf-fdc-setting .fdc-button-posts').generate_posts();

    // Generate comments
    $('.pf-fdc-setting .fdc-button-comments').generate_comments();

    // Generate menus
    $('.pf-fdc-setting .fdc-button-menus').generate_menus();

    // Delete
    $('.pf-fdc-setting .fdc-button-delete').generate_delete();

    // Help the 'quantity' field to generate exact or random
    fdc_active_exact_or_random()
    $('.fdc-date-range').fdc_set_range_date();

    // Help that the meta name has no blank space
    fdc_input_fix_meta_name_space();
    $('.pf-fdc-setting .pf-field-accordion .class-meta .pf-repeater-clone').refresh_validate_name_meta();
  });

} )( jQuery, window, document );;